import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  mainContainer: {
    width: '100%',
    marginTop: '20px',
    paddingTop: '20px',
    backgroundColor: theme.palette.background.paper,
  },
  showMoreReviewsContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    marginTop: '20px',
    marginBottom: '20px'
  }
}));

export default useStyles;
