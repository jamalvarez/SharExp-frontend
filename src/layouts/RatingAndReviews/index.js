import React, { useEffect, useState } from 'react';
import {
  List,
  Container,
  Divider,
  Button, Snackbar
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import RatingAndReviewItem from '../../components/ratingAndReviews/RatingAndReviewItem';
import AverageRating from '../../components/ratingAndReviews/AverageRating';
import SubmitFormModal from '../../components/ratingAndReviews/SubmitFormModal';
import Header from '../../components/ratingAndReviews/Header';
import useStyles from './styles';
import SummaryAndFilters from '../../components/ratingAndReviews/SummaryAndFilters';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function RatingAndReviews() {
  const classes = useStyles();
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [limit, setLimit] = useState(5);
  const [sortBy, setSortBy] = useState('most-recent');
  const [averageRating, setAverageRating] = useState(false);
  const [averageRatings, setAverageRatings] = useState(false);
  const [totalNumberOfReviews, setTotalNumberOfReviews] = useState(0);
  const [totalNumberOfComments, setTotalNumberOfComments] = useState(false);
  const [data, setData] = useState([]);
  const [success, setSuccess] = useState(false);
  const handleOpenModal = () => setModalIsOpen(true);
  const handleCloseModal = () => setModalIsOpen(false);
  const entityId = window.location.pathname.split('/')[2];

  const getAverageRatings = () => {
    fetch(`http://localhost:3000/v2/averageRatings?entityId=${entityId}`)
      .then((response) => response.json())
      .then((result) => setAverageRatings(result));
  };

  const getReviews = () => {
    fetch(`http://127.0.0.1:3000/reviews?entityId=${entityId}&limit=${limit}&sortBy=${sortBy}`)
      .then((response) => response.json())
      .then((result) => {
        setData(result.reviews);
        setAverageRating(result.averageRating);
        setTotalNumberOfReviews(result.totalNumberOfReviews);
        setTotalNumberOfComments(result.totalNumberOfComments);
      });
  }

  useEffect(() => {
    getAverageRatings();
  }, []);

  useEffect(() => {
    getReviews();
  }, [sortBy, limit]);

  const loadMoreReviews = () => {
    setLimit(limit + 5);
  };
  return (
    <Container maxWidth="lg" className={classes.mainContainer}>
      <Header handleOpenModal={handleOpenModal} />
      <SubmitFormModal
        getAverageRatings={getAverageRatings}
        modalIsOpen={modalIsOpen}
        handleCloseModal={handleCloseModal}
        setSuccess={setSuccess}
      />
      {success && (
      <Snackbar open autoHideDuration={3000}>
        <Alert severity="success">
          thank you!
        </Alert>
      </Snackbar>
      )}
      <Divider />
      <AverageRating
        averageRating={averageRating}
        totalNumberOfReviews={totalNumberOfReviews}
      />
      <SummaryAndFilters
        totalNumberOfComments={totalNumberOfComments}
        totalNumberOfReviews={totalNumberOfReviews}
        averageRatings={averageRatings}
        data={data}
        sortByHandler={setSortBy}
      />
      <Divider />
      <List>
        {data.map((item) => <RatingAndReviewItem item={item} key={item} getReviews={getReviews} />)}
        <div className={classes.showMoreReviewsContainer}>
          <Button size="large" variant="outlined" onClick={loadMoreReviews}>Show more Reviews</Button>
        </div>
      </List>
    </Container>
  );
}

export default RatingAndReviews;
