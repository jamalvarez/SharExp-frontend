import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  average: {
    paddingLeft: '30px'
  }
}));

export default useStyles;
