import React from 'react';
import { Box } from '@material-ui/core';
import { Rating } from '@material-ui/lab';
import PropTypes from 'prop-types';
import useStyles from './styles';

const labels = {
  0.5: 'Useless',
  1: 'Useless+',
  1.5: 'Poor',
  2: 'Poor+',
  2.5: 'Ok',
  3: 'Ok+',
  3.5: 'Good',
  4: 'Good+',
  4.5: 'Excellent',
  5: 'Excellent+',
};

function HoverRating({
  size, showFeedBack, rating, averageRating, totalNumberOfReviews
}) {
  const classes = useStyles();
  const [hover] = React.useState(-1);
  const average = (
    <p className={classes.average}>
      {`${averageRating} / 5`}
    </p>
  );

  const commentsCounter = (
    <div className={classes.average}>
      {`${totalNumberOfReviews} comments`}
    </div>
  );

  const ratingValue = (Math.round(averageRating * 10) / 10) || rating;

  return (
    <div className={classes.root}>
      <Rating
        size={size}
        name="hover-feedback"
        value={ratingValue}
        readOnly
        precision={0.1}
      />
      {averageRating && average}
      {totalNumberOfReviews && commentsCounter}
      {
        showFeedBack
        && averageRating !== null
        && <Box ml={2}>{labels[hover !== -1 ? hover : averageRating]}</Box>
      }
    </div>
  );
}

HoverRating.propTypes = {
  size: PropTypes.string,
  showFeedBack: PropTypes.bool,
  averageRating: PropTypes.number,
  totalNumberOfReviews: PropTypes.number,
  rating: PropTypes.number
};

export default HoverRating;
