import React from 'react';
import { Typography } from '@material-ui/core';
import HoverRating from '../../HoverRating';
import useStyles from './styles';

function GeneralQualification() {
  const classes = useStyles();

  return (
    <div>
      <Typography
        component="span"
        variant="h4"
        className={classes.title}
        color="textPrimary"
      >
        General Qualification
      </Typography>
      <HoverRating size="large" showFeedBack={false} showAverage showCommentsCounter />
    </div>
  );
}

export default GeneralQualification;
