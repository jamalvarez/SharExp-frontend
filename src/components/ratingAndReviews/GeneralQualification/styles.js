import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  title: {
    marginTop: '20px',
    marginBottom: '20px',
    display: 'block'
  }
}));

export default useStyles;
