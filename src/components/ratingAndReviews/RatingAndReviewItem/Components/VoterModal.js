import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { CircularProgress } from '@material-ui/core';
import useStyles from '../styles';

function VoterModal({
  setModalIsOpen, saveHandler, setVoterId, voterId, loading
}) {
  const classes = useStyles();
  const closeModalHandler = () => {
    if (loading) return;
    setModalIsOpen(false);
  };
  const handleInputChange = (e) => {
    const voterIdValue = e.target.value;
    setVoterId(voterIdValue);
  };
  return (
    <Dialog open onClose={closeModalHandler} aria-labelledby="form-dialog-title">
      {loading && (
      <div className={classes.loading}>
        <CircularProgress />
      </div>
      )}
      <DialogTitle id="form-dialog-title">Important!</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Please input your name
        </DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          defaultValue={voterId}
          id="name"
          onChange={handleInputChange}
          label="Name"
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={closeModalHandler} color="primary">
          Cancel
        </Button>
        <Button onClick={saveHandler} disabled={!voterId} color="primary">
          Ok!
        </Button>
      </DialogActions>
    </Dialog>
  );
}
VoterModal.propTypes = {
  setModalIsOpen: PropTypes.func.isRequired,
  saveHandler: PropTypes.func.isRequired,
  setVoterId: PropTypes.func.isRequired,
  voterId: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired
};

export default VoterModal;
