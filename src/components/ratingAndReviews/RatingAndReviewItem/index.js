import React from 'react';
import PropTypes from 'prop-types';
import {
  Avatar,
  Button,
  Divider,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Typography,
  Snackbar,
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';

// import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import HoverRating from '../../HoverRating';
import useStyles from './styles';
import VoterModal from './Components/VoterModal';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function RatingAndReviewsItem({ item, getReviews }) {
  const classes = useStyles();
  const [modalIsOpen, setModalIsOpen] = React.useState(false);
  const [voterId, setVoterId] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [failure, setFailure] = React.useState(false);
  const [option, setOption] = React.useState(null);

  const likeHandler = (likeOption) => {
    setVoterId(null);
    setModalIsOpen(true);
    setOption(likeOption);
  };

  const saveHandler = () => {
    setLoading(true);
    const data = {
      voterId,
      voteType: option,
      reviewId: item.reviewId
    };
    fetch('http://localhost:3000/voteReview', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        if (result.code === 400) {
          setModalIsOpen(false);
          setLoading(false);
          setFailure(true);
          setInterval(() => {
            setFailure(false);
          }, 3000);
        } else {
          getReviews();
          setModalIsOpen(false);
          setLoading(false);
          setSuccess(true);
          setInterval(() => {
            setSuccess(false);
          }, 3000);
        }
      });
  };
  return (
    <>
      <ListItem alignItems="flex-start" key={item}>
        {modalIsOpen && (
        <VoterModal
          loading={loading}
          voterId={voterId}
          setVoterId={setVoterId}
          setModalIsOpen={setModalIsOpen}
          saveHandler={saveHandler}
        />
        )}
        {success && (
        <Snackbar open autoHideDuration={3000}>
          <Alert severity="success">
            thank you!
          </Alert>
        </Snackbar>
        )}
        {failure && (
          <Snackbar open autoHideDuration={3000}>
            <Alert severity="error">
              your name can not be empty!
            </Alert>
          </Snackbar>
        )}
        <ListItemAvatar>
          <Avatar alt={item.author} src="/static/images/avatar/1.jpg" />
        </ListItemAvatar>
        <ListItemText
          primary={item.title}
          className={classes.listItemTexts}
          secondary={(
            <>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
                gutterBottom
              >
                {`By ${item.author}`}
              </Typography>
              <HoverRating rating={item.rating} showFeedBack />
              {/* <Box display="flex" alignItems="center" className={classes.itRecomends}> */}
              {/*  <ThumbUpIcon color="primary" /> */}
              {/*  <Typography */}
              {/*    component="span" */}
              {/*    variant="body2" */}
              {/*    className={classes.recomendText} */}
              {/*    color="textPrimary" */}
              {/*  > */}
              {/*    Yes, I recommend this product */}
              {/*  </Typography> */}
              {/* </Box> */}
              <Typography
                component="span"
                variant="body2"
                className={classes.comment}
                color="textPrimary"
              >
                {item.reviewText}
              </Typography>
              <br />
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                Was this comment helpful to you?
              </Typography>
              <Button
                classvariant="outlined"
                size="small"
                color="primary"
                onClick={() => likeHandler('isHelpful')}
              >
                Yes - {item.helpfulnessCount}
              </Button>
              <Button
                variant="outlined"
                size="small"
                color="secondary"
                onClick={() => likeHandler('isAbusive')}
              >
                Report Abuse - {item.reportAbuseCount}
              </Button>
            </>
          )}
        />
      </ListItem>
      <Divider variant="inset" component="li" />
    </>
  );
}

RatingAndReviewsItem.propTypes = {
  item: PropTypes.any,
};

export default RatingAndReviewsItem;
