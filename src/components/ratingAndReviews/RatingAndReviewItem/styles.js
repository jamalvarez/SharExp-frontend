import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    // maxWidth: '36ch',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  comment: {
    color: 'grey',
    marginTop: '5px',
    fontSize: '17px',
    paddingBottom: '5px'
  },
  itRecomends: {
    height: '30px',
  },
  recomendText: {
    paddingLeft: '10px'
  },
  loading: {
    background: '#00000057',
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    zIndex: 3,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
}));

export default useStyles;
