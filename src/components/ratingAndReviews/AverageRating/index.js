import React from 'react';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import HoverRating from '../../HoverRating';
import useStyles from './styles';

function AverageRating({
  totalNumberOfReviews,
  averageRatings
}) {
  const classes = useStyles();

  return (
    <div>
      <Typography
        component="span"
        variant="h4"
        className={classes.title}
        color="textPrimary"
      >
        {`Average Rating | Total Count of Ratings ${totalNumberOfReviews}`}
      </Typography>
      {averageRatings && (
      <HoverRating
        size="large"
        showFeedBack={false}
        averageRating={averageRatings}
        totalNumberOfReviews={totalNumberOfReviews}
      />
      )}

    </div>
  );
}
AverageRating.propTypes = {
  averageRating: PropTypes.number.isRequired,
  totalNumberOfReviews: PropTypes.number.isRequired
};

export default AverageRating;
