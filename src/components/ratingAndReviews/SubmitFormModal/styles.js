import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1)
    },
  },
  field: {
    width: '47%'
  },
  checkbox: {
    margin: '11px'
  },
  title: {
    '& h2': {
      fontSize: '25px'
    }
  },
  ratingCont: {
    margin: '43px',
    paddingLeft: '50px'
  },
  loading: {
    background: '#00000057',
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    zIndex: 3,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
}));

export default useStyles;
