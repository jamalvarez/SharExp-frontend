import React from 'react';
import PropTypes from 'prop-types';
import {
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  Button,
  TextField,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  Typography,
  Box,
  DialogTitle,
  CircularProgress
} from '@material-ui/core';
import { Rating } from '@material-ui/lab';
import useStyles from './styles';

// const labels = {
//   0.5: 'Useless',
//   1: 'Useless+',
//   1.5: 'Poor',
//   2: 'Poor+',
//   2.5: 'Ok',
//   3: 'Ok+',
//   3.5: 'Good',
//   4: 'Good+',
//   4.5: 'Excellent',
//   5: 'Excellent+',
// };

function SubmitFormModal({
  modalIsOpen,
  handleCloseModal,
  getAverageRatings,
  setSuccess
}) {
  const classes = useStyles();
  const entityId = window.location.pathname.split('/')[2];
  const [loading, setLoading] = React.useState(false);
  const [value, setValue] = React.useState('female');
  const [state, setState] = React.useState({
    agree: false,
    entityId,
    title: null,
    description: null,
    author: null
  });
  // const [state, setState] = React.useState({
  //   agree: false,
  //   entityId,
  //   title: null,
  //   description: null
  // });
  const [hover] = React.useState(-1);
  const [rating, setRating] = React.useState(null);

  const closeModal = () => {
    if (loading) return;
    handleCloseModal();
  };

  const handleCheckBoxChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const handleInputChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.value });
  };

  const submitReview = (e) => {
    e.preventDefault();
    const {
      title,
      description,
      author
    } = state;
    if (!rating) return false;
    setLoading(true);
    const data = {
      author,
      entityId,
      rating,
      title,
      description
    };

    fetch('http://localhost:3000/ratingsAndReviews', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then((response) => response.json())
      .then((result) => {
        setLoading(false);
        setSuccess(true);
        setInterval(() => {
          setSuccess(false);
        }, 3000);
        closeModal();
        getAverageRatings();
      });
  };

  return (
    <Dialog open={modalIsOpen} onClose={closeModal} aria-labelledby="form-dialog-title">
      {loading && (
      <div className={classes.loading}>
        <CircularProgress />
      </div>
      )}

      <DialogTitle id="form-dialog-title" className={classes.title}>Your opinion matters to us!</DialogTitle>
      <form className={classes.root} validate onSubmit={submitReview} autoComplete="off">
        <DialogContent>
          <DialogContentText>
            Evaluate your &quot;Macbook Air 13.3 Intel Core I5 8gb Ram 128gb Ssd&quot;!
            Required fields are marked with *
          </DialogContentText>
          <FormControl row>
            <TextField
              required
              id="outlined-required"
              label="Name"
              name="author"
              defaultValue=""
              variant="outlined"
              placeholder="Jhon Doe"
              onChange={handleInputChange}
            />
            <TextField
              required
              id="outlined-required"
              label="Product ID"
              name="entityId"
              defaultValue={entityId}
              variant="outlined"
              onChange={handleInputChange}
              placeholder="12322"
            />
          </FormControl>
          <FormControl component="fieldset" mb={3} borderColor="transparent" className={classes.ratingCont}>
            <Typography component="legend">General Rating</Typography>
            <Rating
              size="large"
              name="rating-input"
              required
              value={rating}
              precision={1}
              onChange={(event, newValue) => {
                setRating(newValue);
              }}
            />
            {/* {rating !== null */}
            {/* && <Box ml={2}>{labels[hover !== -1 ? hover : rating]}</Box>} */}
          </FormControl>
          <TextField
            required
            id="outlined-required"
            label="Title"
            name="title"
            defaultValue=""
            onChange={handleInputChange}
            variant="outlined"
            placeholder="Example: I can take it everywhere"
            fullWidth
          />
          <TextField
            label="Comment"
            rowsMax={4}
            placeholder="It is very easy to move, it is light and opens easily ..."
            variant="outlined"
            name="description"
            onChange={handleInputChange}
            fullWidth
          />
          {/* <FormControl component="fieldset" row className={classes.checkbox}> */}
          {/*  <FormLabel component="legend">¿Do you like this product?</FormLabel> */}
          {/*  <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}> */}
          {/*    <FormControlLabel value="yes" control={<Radio />} label="Yes" /> */}
          {/*    <FormControlLabel value="no" control={<Radio />} label="No" /> */}
          {/*  </RadioGroup> */}
          {/* </FormControl> */}

          <FormControlLabel
            control={(
              <Checkbox
                checked={state.agree}
                onChange={handleCheckBoxChange}
                name="agree"
                color="primary"
              />
          )}
            label="I accept the terms and conditions"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={closeModal} color="primary">
            Back
          </Button>
          <Button type="submit" disabled={!state.agree} color="primary">
            Post comment
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}

SubmitFormModal.propTypes = {
  modalIsOpen: PropTypes.bool.isRequired,
  handleCloseModal: PropTypes.func.isRequired,
  getAverageRatings: PropTypes.func.isRequired,
  setSuccess: PropTypes.func.isRequired
};

export default SubmitFormModal;
