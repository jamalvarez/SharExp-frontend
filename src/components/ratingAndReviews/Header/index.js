import React from 'react';
import PropTypes from 'prop-types';
import { Button, Typography } from '@material-ui/core';
import useStyles from './styles';

const Header = ({ handleOpenModal }) => {
  const classes = useStyles();
  return (
    <div className={classes.header}>
      <Typography variant="h4" className={classes.title}>
        Comments
      </Typography>
      <Button variant="outlined" color="primary" onClick={handleOpenModal}>
        Write a comment
      </Button>
    </div>
  );
};

Header.propTypes = {
  handleOpenModal: PropTypes.func.isRequired
};

export default Header;
