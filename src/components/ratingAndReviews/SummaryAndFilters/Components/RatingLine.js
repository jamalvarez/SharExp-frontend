import React from 'react';
import PropTypes from 'prop-types';
import StarsSharpIcon from '@material-ui/icons/StarRate';
import { withStyles } from '@material-ui/core/styles';
import { LinearProgress } from '@material-ui/core';
import useStyles from '../styles';

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 15,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#FEBF00',
  },
}))(LinearProgress);

const RatingLine = ({ value, amount, total }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      {value}
      {' '}
      <StarsSharpIcon color="primary" className={classes.start} />
      {' '}
      <BorderLinearProgress variant="determinate" value={(amount * 100) / total} className={`${classes.progress}`} />
      {'  '}
      {amount}
    </div>
  );
};

RatingLine.propTypes = {
  value: PropTypes.number.isRequired,
  amount: PropTypes.number,
  total: PropTypes.number,
};

export default RatingLine;
