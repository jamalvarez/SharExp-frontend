import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: '20px',
    marginBottom: '20px'
  },
  start: {
    color: '#FEBF00'
  },
  root: {
    flexGrow: 1,
    display: 'flex',
    height: '29px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  progress: {
    width: '290px',
    display: 'inline-block',
    marginRight: '10px'
  },
  counterFilterContainer: {
    marginTop: '20px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  resumeContainer: {
    maxWidth: '350px',
  }
}));

export default useStyles;
