import React from 'react';
import { countBy } from 'lodash';
import {
  Typography
} from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';

import PropTypes from 'prop-types';
import useStyles from './styles';
import RatingLine from './Components/RatingLine';

const SummaryAndFilters = ({ data, sortByHandler, averageRatings, totalNumberOfComments, totalNumberOfReviews }) => {
  const classes = useStyles();

  const [filterBy, setFilterBy] = React.useState({
    value: 'most-recent',
    name: 'sortBy',
  });

  const reviewsCounter = countBy(data, 'rating');
  console.log('reviewsCounter', reviewsCounter);
  let sum = 0;
  Object.keys(reviewsCounter).forEach((review) => {
    sum += reviewsCounter[review];
  });

  const handleChange = (event) => {
    sortByHandler(event.target.value);
    setFilterBy({
      name: 'sortBy',
      value: event.target.value,
    });
  };
  return (
    <div className={classes.container}>
      <Typography
        component="span"
        variant="h4"
        className={classes.title}
        color="textPrimary"
      >
        Select a note to view comments
      </Typography>
      {sum > 0 && (
      <div className={classes.resumeContainer}>
        <RatingLine
          value={5}
          total={averageRatings.totalNumberOfReviews}
          amount={averageRatings.numberOfFiveStarReviews}
        />
        <RatingLine
          value={4}
          total={averageRatings.totalNumberOfReviews}
          amount={averageRatings.numberOfFourStarReviews}
        />
        <RatingLine
          value={3}
          total={averageRatings.totalNumberOfReviews}
          amount={averageRatings.numberOfThreeStarReviews}
        />
        <RatingLine
          value={2}
          total={averageRatings.totalNumberOfReviews}
          amount={averageRatings.numberOfTwoStarReviews}
        />
        <RatingLine
          value={1}
          total={averageRatings.totalNumberOfReviews}
          amount={averageRatings.numberOfOneStarReviews}
        />
      </div>
      )}
      <div className={classes.counterFilterContainer}>
        <div className={classes.counter}>
          {`1–${data.length} de ${totalNumberOfComments} Comments`}

        </div>
        <div className={classes.filter}>
          <FormControl className={classes.formControl}>
            <InputLabel shrink htmlFor="age-native-label-placeholder">
              Sort By
            </InputLabel>
            <NativeSelect
              value={filterBy.value}
              onChange={handleChange}
              inputProps={{
                name: 'sortBy',
                id: 'age-native-label-placeholder',
              }}
            >
              <option value="most-recent">Most recent</option>
              <option value="highest-rated">Highest rated</option>
              <option value="lowest-rated">Lowest rated</option>
              <option value="most-helpful">Most Helpful</option>
              <option value="most-positive">Most Positive</option>
              <option value="most-negative">Most Negative</option>
            </NativeSelect>
          </FormControl>
        </div>
      </div>
    </div>
  );
};

SummaryAndFilters.propTypes = {
  data: PropTypes.any,
  sortByHandler: PropTypes.func.isRequired,
  totalNumberOfComments: PropTypes.number.isRequired,
  averageRatings: PropTypes.shape({
    numberOfFiveStarReviews: PropTypes.number.isRequired,
    numberOfFourStarReviews: PropTypes.number.isRequired,
    numberOfThreeStarReviews: PropTypes.number.isRequired,
    numberOfTwoStarReviews: PropTypes.number.isRequired,
    numberOfOneStarReviews: PropTypes.number.isRequired,
    totalNumberOfReviews: PropTypes.number.isRequired
  }).isRequired,
};

export default SummaryAndFilters;
