import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2)
  },
  textContainer: {
    display: 'block',
    width: '200px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  }
}));


const Results = ({ className, handleSetLimit, setSelectedReviews, handleSetOffset, customers, ...rest }) => {
  const classes = useStyles();
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(1);


  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.reviewId);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
    setSelectedReviews(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds, id);
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds.slice(1));
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }
    setSelectedReviews(newSelectedCustomerIds);
    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
    handleSetLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
    handleSetOffset(newPage);
  };

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedCustomerIds.length === customers.length}
                    color="primary"
                    indeterminate={
                      selectedCustomerIds.length > 0
                      && selectedCustomerIds.length < customers.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                <TableCell>
                  Author
                </TableCell>
                <TableCell>
                  Rating
                </TableCell>
                <TableCell>
                  Title
                </TableCell>
                <TableCell>
                  Review Text
                </TableCell>
                <TableCell>
                  isPurchaseVerified
                </TableCell>
                <TableCell>
                  profanityAnalysisScore
                </TableCell>
                <TableCell>
                  sentimentAnalysisScore
                </TableCell>
                <TableCell>
                  helpfulnessCount
                </TableCell>
                <TableCell>
                  reportAbuseCount
                </TableCell>
                <TableCell>
                  Registration date
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers.slice(0, limit).map((customer) => (
                <TableRow
                  hover
                  key={customer.reviewId}
                  selected={selectedCustomerIds.indexOf(customer.reviewId) !== -1}
                >
                  <TableCell padding="checkbox">
                    <Checkbox
                      checked={selectedCustomerIds.indexOf(customer.reviewId) !== -1}
                      onChange={(event) => handleSelectOne(event, customer.reviewId)}
                      value="true"
                    />
                  </TableCell>
                  <TableCell>
                    <Box
                      alignItems="center"
                      display="flex"
                    >
                      {/* <Avatar */}
                      {/*  className={classes.avatar} */}
                      {/*  src={customer.avatarUrl} */}
                      {/* > */}
                      {/*  {getInitials(customer.author)} */}
                      {/* </Avatar> */}
                      <Typography
                        color="textPrimary"
                        variant="body1"
                      >
                        {customer.author}
                      </Typography>
                    </Box>
                  </TableCell>
                  <TableCell>
                    {customer.rating}
                  </TableCell>
                  <TableCell>
                    {customer.title}
                  </TableCell>
                  <TableCell component="th" scope="row" alt={customer.reviewText}>
                    <div className={classes.textContainer}>
                      {customer.reviewText}
                    </div>
                  </TableCell>
                  <TableCell>
                    {customer.isPurchaseVerified}
                  </TableCell>
                  <TableCell>
                    {customer.profanityAnalysisScore}
                  </TableCell>
                  <TableCell>
                    {customer.sentimentAnalysisScore}
                  </TableCell>
                  <TableCell>
                    {customer.helpfulnessCount}
                  </TableCell>
                  <TableCell>
                    {customer.reportAbuseCount}
                  </TableCell>
                  <TableCell>
                    {moment(customer.reviewedDateTime).format('DD/MM/YYYY')}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={customers.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired,
  handleSetLimit: PropTypes.func.isRequired,
};

export default Results;
