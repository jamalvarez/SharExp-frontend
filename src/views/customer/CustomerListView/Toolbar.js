import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  makeStyles, Typography
} from '@material-ui/core';
import { Search as SearchIcon } from 'react-feather';
import InputLabel from '@material-ui/core/InputLabel';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormControl from '@material-ui/core/FormControl';

const useStyles = makeStyles((theme) => ({
  root: {},
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  },
  formControl: {
    marginLeft: '10px'
  },
  title: {
    fontSize: '35px'
  },
  headerCont: {
    padding: '0 30px 0 30px'
  }
}));

const Toolbar = ({
  className, handleApprove, handleReject, filter, setFilter, ...rest
}) => {
  const classes = useStyles();

  const filterChangeHandler = (event) => {
    setFilter(event.target.value);
  };

  return (
    <div
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Box
        display="flex"
        justifyContent="space-between"
        className={classes.headerCont}
      >
        <Typography
          component="span"
          variant="h4"
          className={classes.title}
          color="textPrimary"
        >
          Customer Moderation
        </Typography>
        <div>
          <FormControl className={classes.formControl}>
            <InputLabel shrink htmlFor="age-native-label-placeholder">
              Fetch By
            </InputLabel>
            <NativeSelect
              value={filter}
              onChange={filterChangeHandler}
              inputProps={{
                name: 'sortBy',
                id: 'age-native-label-placeholder',
              }}
            >
              <option value="pending">Pending</option>
              <option value="approved">Approved</option>
              <option value="rejected">Rejected</option>
            </NativeSelect>
          </FormControl>
          <Button
            color="primary"
            variant="contained"
            onClick={handleApprove}
            className={classes.formControl}
          >
            Approve
          </Button>
          <Button
            color="secondary"
            variant="contained"
            onClick={handleReject}
            className={classes.formControl}
          >
            Reject
          </Button>
        </div>
      </Box>
    </div>
  );
};

Toolbar.propTypes = {
  className: PropTypes.string,
  handleApprove: PropTypes.func.isRequired,
  handleReject: PropTypes.func.isRequired,
  filter: PropTypes.func.isRequired,
  setFilter: PropTypes.func.isRequired
};

export default Toolbar;
