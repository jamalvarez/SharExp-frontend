import React, { useState, useEffect } from 'react';
import {
  Snackbar,
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import MuiAlert from '@material-ui/lab/Alert';
import Results from './Results';
import Toolbar from './Toolbar';

// import data from './data';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const CustomerListView = () => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [limit, setLimit] = useState(20);
  const [offset, setOffset] = useState(0);
  const [selectedReviews, setSelectedReviews] = useState([]);
  const [success, setSuccess] = useState(false);
  const [filter, setFilter] = useState('pending');
  // const [customers] = useState(data);
  const getAverageRatings = () => {
    fetch(`http://localhost:3000/v2/reviewsByStatus/${filter}?limit=${limit}&offset=${(limit * offset)}`)
      .then((response) => response.json())
      .then((result) => {
        setData(result.reviewData);
      });
  };

  const approve = () => {
    const payload = {
      reviewIds: selectedReviews,
      reviewStatus: 1
    };
    fetch('http://localhost:3000/reviewStatus', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
      .then((response) => response.json())
      .then((result) => {
        getAverageRatings();
        setSuccess(true);
        // setLoading(false);
        // setSuccess(true);
        setInterval(() => {
          setSuccess(false);
        }, 3000);
        // closeModal();
        // getAverageRatings();
      });
  };
  const reject = () => {
    const payload = {
      reviewIds: selectedReviews,
      reviewStatus: 2
    };
    fetch('http://localhost:3000/reviewStatus', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    })
      .then((response) => response.json())
      .then((result) => {
        getAverageRatings();
        setSuccess(true);
        // setLoading(false);
        // setSuccess(true);
        setInterval(() => {
          setSuccess(false);
        }, 3000);
        // closeModal();
        // getAverageRatings();
      });
  };

  useEffect(() => {
    getAverageRatings();
  }, [filter]);

  return (
    <Page
      className={classes.root}
      title="Customers"
    >
      <Container maxWidth={false}>
        <Toolbar handleReject={reject} handleApprove={approve} setFilter={setFilter} filter={filter}/>
        {success && (
          <Snackbar open autoHideDuration={3000}>
            <Alert severity="success">
              Done!
            </Alert>
          </Snackbar>
        )}
        {' '}
        <Box mt={3}>
          <Results
            setSelectedReviews={setSelectedReviews}
            customers={data}
            handleSetLimit={setLimit}
            handleSetOffset={setOffset}
          />
        </Box>
      </Container>
    </Page>
  );
};

export default CustomerListView;
