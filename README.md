# E-commerce - Reviews & Ratings

![Generic badge](https://img.shields.io/badge/USES-JS-yellow.svg?style=for-the-badge)

> UI Project for Falabella Reviews & Ratings

## Prerequisites

- NodeJS v10.16.0 and above
- Mongodb v3.6.2 and above

## Installation

To get started with the project, first **clone** the project from gitlab

```
git@git.fala.cl:hpagadimarri/hackathon2020.git
```

after cloning

```
$ cd hackathon2020
``` 

Package installation

```
npm install
``` 

After the above command runs successfully, the **dependencies** get installed


To Start Server:

```
npm run start
``` 
 
## Swagger Doc
https://app.swaggerhub.com/apis/hpagadimarri/ratings-and_reviews/2.0